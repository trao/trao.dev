#!/bin/bash
cd trao.dev
git checkout master
git pull origin master
docker-compose down
docker-compose build --no-cache
docker-compose up -d --force-recreate
exit