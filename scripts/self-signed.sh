#!/bin/bash

if ! [ -x "$(command -v mkcert)" ]; then
  echo 'Error: mkcert is not installed.' >&2
  exit 1
fi

mkdir -p $DATA_DIR/certbot/conf/live/trao.dev $DATA_DIR/certbot/conf/live/ghost.trao.dev
mkcert -key-file $DATA_DIR/certbot/conf/live/trao.dev/privkey.pem -cert-file $DATA_DIR/certbot/conf/live/trao.dev/fullchain.pem trao.dev *.trao.dev
mkcert -key-file $DATA_DIR/certbot/conf/live/ghost.trao.dev/privkey.pem -cert-file $DATA_DIR/certbot/conf/live/ghost.trao.dev/fullchain.pem ghost.trao.dev *.ghost.trao.dev
mkcert -install