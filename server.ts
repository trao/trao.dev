import { Request, Response } from 'express';

/* eslint-disable @typescript-eslint/no-var-requires */
const express = require('express');
const { v4 } = require('uuid');
const next = require('next');
const cookieParser = require('cookie-parser');
/* eslint-enable @typescript-eslint/no-var-requires */

const PORT = process.env.PORT || 3000;
const dev = process.env.NODE_ENV !== 'production';
const app = next({ dev });
const handle = app.getRequestHandler();
const uuidv4 = v4;

/* eslint-disable @typescript-eslint/no-explicit-any,@typescript-eslint/explicit-function-return-type */
function sessionCookie(req: Request, res: Response, next: any) {
  const htmlPage =
    !req.path.match(/^\/(_next|static)/) &&
    !req.path.match(/\.(js|map)$/) &&
    req.accepts('text/html', 'text/css', 'image/png') === 'text/html';

  if (!htmlPage) {
    next();
    return;
  }

  if (!req.cookies.sid || req.cookies.sid.length === 0) {
    req.cookies.sid = uuidv4();
    res.cookie('sid', req.cookies.sid, {
      maxAge: 3600000 * 24 * 180,
      secure: process.env.NODE_ENV === 'production',
    });
  }

  next();
}

const sourcemapsForSentryOnly = (token: string) => (
  req: Request,
  res: Response,
  next: any
) => {
  // In production we only want to serve source maps for Sentry
  if (!dev && !!token && req.headers['x-sentry-token'] !== token) {
    res
      .status(401)
      .send(
        'Authentication access token is required to access the source map.'
      );
    return;
  }
  next();
};
/* eslint-enable @typescript-eslint/no-explicit-any,@typescript-eslint/explicit-function-return-type */

app
  .prepare()
  .then(() => {
    // eslint-disable-next-line @typescript-eslint/no-var-requires
    const { Sentry } = require('./sentry')(app.buildId);
    const server = express();

    server.disable('x-powered-by');
    server.set('trust proxy', true);
    server.use(Sentry.Handlers.requestHandler());
    server.use(cookieParser());
    server.use(sessionCookie);
    server.get(
      /\.map$/,
      sourcemapsForSentryOnly(process.env.SENTRY_TOKEN as string)
    );
    server.get('*', (req: Request, res: Response) => {
      /**
       * Redirect URLs with trailing slash
       * to without. Next.js does not like them.
       */
      if (
        req.url !== '/' &&
        req.url.endsWith('/') &&
        !req.url.includes('webpack-hmr')
      ) {
        res.redirect(301, req.url.slice(0, -1));
      }
      return handle(req, res);
    });
    server.post('*', (req: Request, res: Response) => {
      return handle(req, res);
    });
    server.use(Sentry.Handlers.errorHandler());

    server.listen(PORT, (err: Error) => {
      if (err) throw err;
      console.log(`> Ready on http://localhost:${PORT}`);
    });
  })
  .catch((error: Error) => {
    throw error;
  });
