import { GetServerSideProps, NextPage } from 'next';
import { getPosts, getPages, getSettings } from '../ghost';

type SiteMapItem = {
  url: string;
  updatedAt?: string | null;
};

const createSitemap = (pages: SiteMapItem[]): string =>
  `<?xml version="1.0" encoding="UTF-8"?>
    <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
      ${pages
        .map(({ url, updatedAt }) => {
          return `
            <url>
                <loc>${`${url}`}</loc>
                ${(updatedAt && `<lastmod>${updatedAt}</lastmod>`) || ''}
            </url>
        `;
        })
        .join('')}
    </urlset>`;

const Sitemap: NextPage = () => {
  return null;
};

export const getServerSideProps: GetServerSideProps = async (context) => {
  const posts = await getPosts();
  const pages = await getPages();
  const siteSettings = await getSettings();
  const { navigation } = siteSettings;
  const siteMapLinks: SiteMapItem[] = [
    {
      url: `${process.env.NEXT_HOSTNAME}`,
    },
  ];

  if (navigation) {
    navigation.forEach(({ url }) => {
      if (!url.includes('http') && !url.includes('page/about')) {
        siteMapLinks.push({
          url: `${process.env.NEXT_HOSTNAME}${url}`,
        });
      }
    });
  }

  if (posts) {
    // add post links
    posts.forEach((post) => {
      siteMapLinks.push({
        url: `${process.env.NEXT_HOSTNAME}/post/${post.slug}`,
        updatedAt: post.updated_at,
      });
    });
  }

  if (pages) {
    // add page links
    pages.forEach((page) => {
      siteMapLinks.push({
        url: `${process.env.NEXT_HOSTNAME}/page/${page.slug}`,
        updatedAt: page.updated_at,
      });
    });
  }

  context.res.setHeader('Content-Type', 'text/xml');
  context.res.write(createSitemap(siteMapLinks));
  context.res.end();

  return {
    props: {},
  };
};

export default Sitemap;
