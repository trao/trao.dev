import * as React from 'react';
import { NextPage } from 'next';
import { PostOrPage } from '@tryghost/content-api';
import {
  Flex,
  Text,
  Card,
  AspectImage,
  Box,
  Avatar,
  SxStyleProp,
} from 'theme-ui';
import {
  FiTwitter,
  FiGithub,
  FiGitlab,
  FiLinkedin,
  FiInstagram,
} from 'react-icons/fi';
import Link from '../components/Link';
import { useAnalytics } from 'use-analytics';

export type IndexProps = {
  posts: PostOrPage[] | void;
};

const socialIconStyles: SxStyleProp = {
  display: 'inline-block',
  marginTop: 2,
  marginRight: 3,
  color: 'text',
};

const Index: NextPage<IndexProps> = () => {
  const analytics = useAnalytics();

  React.useEffect(() => {
    analytics.page();
  }, []);

  return (
    <Flex marginTop={4} sx={{ justifyContent: 'center', alignItems: 'center' }}>
      <Card sx={{ width: ['100%', '50%'] }}>
        <AspectImage
          src="images/cover.jpg"
          ratio={16 / 5}
          sx={{ borderTopLeftRadius: 3, borderTopRightRadius: 3 }}
        />
        <Flex padding={3}>
          <Box>
            <Avatar
              src="https://www.gravatar.com/avatar/09388e353d3181176ae954ce586f7c7e"
              sx={{
                height: 80,
                width: 80,
                transform: 'translateY(-45px)',
                border: '2px solid',
                borderColor: 'white',
              }}
            />
          </Box>
          <Box marginLeft={3} sx={{ flex: 1 }}>
            <Text variant="subhead">Thilak Rao</Text>
            <Text variant="muted">Frontend Developer at Booking.com</Text>
            <Link href="https://twitter.com/thilak" sx={socialIconStyles}>
              <FiTwitter size={25} title="Thilak Rao on Twitter" />
            </Link>
            <Link href="https://github.com/xthilakx" sx={socialIconStyles}>
              <FiGithub size={25} title="Thilak Rao on Github" />
            </Link>
            <Link href="https://gitlab.com/trao" sx={socialIconStyles}>
              <FiGitlab size={25} title="Thilak Rao on Gitlab" />
            </Link>
            <Link
              href="https://www.linkedin.com/in/trao/"
              sx={socialIconStyles}
            >
              <FiLinkedin size={25} title="Thilak Rao on LinkedIn" />
            </Link>
            <Link href="http://instagram.com/_trao" sx={socialIconStyles}>
              <FiInstagram size={25} title="Thilak Rao on Instagram" />
            </Link>
          </Box>
        </Flex>
      </Card>
    </Flex>
  );
};

export default Index;
