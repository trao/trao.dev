import * as React from 'react';
import { getSinglePost } from '../../ghost';
import dynamic from 'next/dynamic';
import { useRouter } from 'next/router';
import { GetServerSideProps, NextPage } from 'next';
import { PostOrPage } from '@tryghost/content-api';
import { Box, Heading } from 'theme-ui';
import { parseExcerpt, parser } from '../../utils';
import { Article, WithContext } from 'schema-dts';
import { useAnalytics } from 'use-analytics';

// For syntax highlighting
import { highlightAll } from 'prismjs';
import 'prismjs/components/prism-css-extras.min';
import 'prismjs/components/prism-typescript.min';
import 'prismjs/components/prism-jsx.min';
import 'prismjs/components/prism-tsx.min';

const Custom404 = dynamic(() => import('../404'));
const Meta = dynamic(() => import('../../components/Meta'));
const Comments = dynamic(() => import('../../components/Comments'), {
  ssr: false,
});

const makePostSchema = (
  post: PostOrPage,
  fullUrl: string
): WithContext<Article> => {
  const context = 'https://schema.org';

  return {
    '@context': context,
    '@type': 'Article',
    mainEntityOfPage: {
      '@type': 'WebPage',
      '@id': fullUrl,
    },
    url: fullUrl,
    image: [post.feature_image as string],
    publisher: {
      '@type': 'Person',
      name: post.primary_author?.name,
      image: {
        '@type': 'ImageObject',
        url: post.primary_author?.profile_image as string,
        width: '192',
        height: '192',
      },
    },
    headline: post.title,
    author: {
      '@type': 'Person',
      url: post.primary_author?.url as string,
      name: post.primary_author?.name,
    },
    datePublished: (post.published_at || post.created_at) as string,
    dateModified: (post.updated_at || post.created_at) as string,
  };
};

type PostProps = {
  post: PostOrPage | null;
};

const Post: NextPage<PostProps> = ({ post }) => {
  const { asPath } = useRouter();
  const analytics = useAnalytics();
  const fullUrl = `${process.env.NEXT_HOSTNAME}${asPath}`;

  if (!post || !post?.html) {
    return <Custom404 />;
  }

  /* eslint-disable @typescript-eslint/camelcase */
  const {
    title,
    twitter_title,
    twitter_description,
    twitter_image,
    meta_title,
    meta_description,
    html,
    excerpt,
    feature_image,
    og_description,
    og_image,
    og_title,
    uuid,
    slug,
  } = post;

  React.useEffect(() => {
    highlightAll();
    analytics.page({
      title,
    });
  }, []);

  return (
    <>
      <Meta
        title={meta_title || title}
        description={meta_description || parseExcerpt(excerpt, 160)}
        twitterDescription={
          twitter_description || meta_description || parseExcerpt(excerpt, 200)
        }
        twitterImage={twitter_image || feature_image}
        twitterTitle={twitter_title || title}
        featureImage={feature_image}
        fbImage={og_image || feature_image}
        fbDescription={
          og_description || meta_description || parseExcerpt(excerpt, 200)
        }
        fbTitle={og_title || title}
        structuredData={makePostSchema(post, fullUrl)}
      />

      <Heading as="h1" mb={3}>
        {title}
      </Heading>
      <Box as="article">{parser(html)}</Box>

      {title && uuid && (
        <Comments title={title} shortname={slug} uuid={uuid} marginTop={4} />
      )}
    </>
  );
  /* eslint-enable @typescript-eslint/camelcase */
};

export const getServerSideProps: GetServerSideProps<PostProps> = async ({
  res,
  params,
}) => {
  const post = await getSinglePost(params?.slug as string);

  if (!post && res) {
    res.statusCode = 404;
  }

  return {
    props: {
      post: post || null,
    },
  };
};

export default Post;
