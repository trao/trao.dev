/** @jsx jsx */
import * as React from 'react';
import Head from 'next/head';
import { jsx, Image, Box, Heading } from 'theme-ui';
import { AnalyticsActions, AnalyticsCategory } from '../utils/analytics';
import { useRouter } from 'next/router';
import { useAnalytics } from 'use-analytics';

const gifs = [
  'https://media.giphy.com/media/l1J9EdzfOSgfyueLm/giphy.gif',
  'https://media.giphy.com/media/YyKPbc5OOTSQE/giphy.gif',
  'https://media.giphy.com/media/14uQ3cOFteDaU/giphy.gif',
  'https://media.giphy.com/media/VwoJkTfZAUBSU/giphy.gif',
  'https://media.giphy.com/media/ZUdJFU0tbEpnW/giphy.gif',
  'https://media.giphy.com/media/8L0Pky6C83SzkzU55a/giphy.gif',
  'https://media.giphy.com/media/dsWOUTBz5aae8ET8Ss/giphy.gif',
  'https://media.giphy.com/media/9J7tdYltWyXIY/giphy.gif',
];

const Custom404: React.FC = () => {
  const { asPath } = useRouter();
  const analytics = useAnalytics();

  React.useEffect(() => {
    analytics.page({
      path: asPath,
    });
    analytics.track(AnalyticsActions.VIEWED, {
      category: AnalyticsCategory.ERROR_404,
      label: asPath,
    });
  }, []);

  return (
    <Box
      marginY={4}
      sx={{
        display: 'flex',
        justifyContent: 'center',
        flexDirection: 'column',
      }}
    >
      <Head>
        <title>Page not found</title>
      </Head>
      <Image src={gifs[Math.floor(Math.random() * gifs.length)]} />
      <Heading marginTop={4} sx={{ textAlign: 'center' }}>
        Page not found
      </Heading>
    </Box>
  );
};

export default Custom404;
