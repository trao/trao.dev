import * as React from 'react';
import { GetServerSideProps, NextPage } from 'next';
import { PostOrPage } from '@tryghost/content-api';
import { getPosts } from '../ghost';
import Link from '../components/Link';
import { Text } from 'theme-ui';
import { parseExcerpt } from '../utils';
import { useAnalytics } from 'use-analytics';

export type PostsProps = {
  posts: PostOrPage[] | void;
};

const Posts: NextPage<PostsProps> = (props) => {
  const analytics = useAnalytics();

  React.useEffect(() => {
    analytics.page();
  }, []);

  if (!props.posts) {
    console.error('Cannot render index. Invalid props!');
    return null;
  }

  const posts = props.posts
    ? props.posts.map((post) => (
        <li key={post.id}>
          <Link href={`/post/[slug]`} as={`/post/${post.slug}`}>
            {post.title}
          </Link>
          <Text>{parseExcerpt(post.excerpt)}</Text>
        </li>
      ))
    : null;

  return <ul>{posts}</ul>;
};

export const getServerSideProps: GetServerSideProps<PostsProps> = async () => {
  const posts = await getPosts();

  return {
    props: {
      posts,
    },
  };
};

export default Posts;
