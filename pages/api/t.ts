import { NextApiRequest, NextApiResponse } from 'next';
// https://github.com/benmosher/eslint-plugin-import/issues/1590
// eslint-disable-next-line import/default
import universalAnalytics from 'universal-analytics';

type UAClient = universalAnalytics.Visitor;
type CommonArgs = {
  userAgent: string;
  hostname: string;
  ipAddress: string;
};

const initialize = (clientId: string): UAClient => {
  if (!process.env.GA_TRACKING_ID) {
    throw new Error('No google analytics trackingId defined');
  }

  return universalAnalytics(process.env.GA_TRACKING_ID, clientId, {
    strictCidFormat: false,
    uid: clientId,
  });
};

type PageViewArgs = {
  path: string;
  url: string;
  title: string;
} & CommonArgs;

const pageView = (
  { path, url, title, hostname, ipAddress, userAgent }: PageViewArgs,
  client: UAClient
): void => {
  if (!path || !url || !title) {
    throw new Error('Missing path, href or title in page call for GA');
  }
  client
    .pageview({
      dp: path,
      dl: url,
      dt: title,
      dh: hostname,
      uip: ipAddress,
      ua: userAgent,
    })
    .send();
};

type EventArgs = {
  category: string;
  event: string;
  label: string;
  value: number;
} & CommonArgs;

const trackEvent = (
  { category, event, label, value, hostname, ipAddress, userAgent }: EventArgs,
  client: UAClient
): void => {
  client
    .event({
      ec: category,
      ea: event,
      el: label,
      ev: value,
      dh: hostname,
      uip: ipAddress,
      ua: userAgent,
    })
    .send();
};

const identifyVisitor = (id: string, client: UAClient): void => {
  client.set('uid', id);
};

export default (req: NextApiRequest, res: NextApiResponse): void => {
  const ipAddress = (req.headers['x-forwarded-for'] ||
    req.connection.remoteAddress) as string;
  const userAgent = req.headers['user-agent'] as string;
  const hostname = req.headers.host as string;
  const commonArgs = { ipAddress, userAgent, hostname };

  if (req.method === 'POST') {
    const client = initialize(req.body.userId);
    switch (req.body.type) {
      case 'identify': {
        identifyVisitor(req.body.userId, client);
        break;
      }
      case 'page': {
        const { path, url, title } = req.body.properties;
        pageView({ path, url, title, ...commonArgs }, client);
        break;
      }
      case 'track': {
        const { event, properties } = req.body.payload;
        const category = properties.category || 'All';
        const label = properties.label || 'NA';
        const value = properties.value;
        trackEvent(
          {
            category,
            event,
            label,
            value,
            ...commonArgs,
          },
          client
        );
        break;
      }
      default:
        res.status(412).end();
        break;
    }
    res.status(200).json({ success: true });
  } else {
    res.status(405).end();
  }
};
