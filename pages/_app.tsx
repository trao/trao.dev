import * as React from 'react';
import { AppInitialProps, AppProps } from 'next/app';
import dynamic from 'next/dynamic';
import { AppContextType } from 'next/dist/next-server/lib/utils';
import { ThemeProvider } from 'theme-ui';
import { SettingsResponse } from '@tryghost/content-api';
import { getSettings } from '../ghost';
import { NavigationItem } from '../components/Header';
import { theme } from '../theme';
import { Router } from 'next/router';
import { AnalyticsProvider } from 'use-analytics';
import { analytics } from '../utils/analytics';
import '../utils/prism.css';
import { cookiesToJson } from '../utils/cookies';

const Layout = dynamic(() => import('../components/Layout'));
const Meta = dynamic(() => import('../components/Meta'));

type CustomAppProps = {
  siteSettings: SettingsResponse;
  navigation: NavigationItem[];
  uuid: string;
  currentPath?: string;
};

/* eslint-disable @typescript-eslint/camelcase */
const CustomApp = (props: CustomAppProps & AppProps): JSX.Element => {
  const {
    Component,
    pageProps,
    navigation,
    siteSettings: {
      meta_title,
      meta_description,
      twitter_title,
      twitter_description,
      twitter_image,
      og_description,
      og_image,
      og_title,
    },
    uuid,
  } = props;

  analytics.identify(uuid);

  return (
    <AnalyticsProvider instance={analytics}>
      <ThemeProvider theme={theme}>
        <Layout navigation={navigation}>
          <Meta
            title={meta_title || ''}
            description={meta_description}
            twitterImage={twitter_image}
            twitterDescription={twitter_description}
            twitterTitle={twitter_title}
            fbImage={og_image}
            fbDescription={og_description}
            fbTitle={og_title}
          />
          <Component {...pageProps} />
        </Layout>
      </ThemeProvider>
    </AnalyticsProvider>
  );
};
/* eslint-enable @typescript-eslint/camelcase */

CustomApp.getInitialProps = async ({
  Component,
  ctx,
}: AppContextType<Router>): Promise<CustomAppProps & AppInitialProps> => {
  const siteSettings = await getSettings();
  const { navigation } = siteSettings;
  const cookies = cookiesToJson(ctx.req?.headers.cookie || '');
  const pageProps = Component.getInitialProps
    ? await Component.getInitialProps(ctx)
    : {};

  return {
    uuid: cookies.sid,
    currentPath: ctx.asPath,
    pageProps,
    siteSettings,
    navigation: (navigation || []).map((nav) => {
      if (nav.url.endsWith('/')) {
        nav.url = nav.url.slice(0, -1);
      }

      return nav;
    }),
  };
};

export default CustomApp;
