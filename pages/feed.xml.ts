import { GetServerSideProps, NextPage } from 'next';
import { getPosts } from '../ghost/posts';
import { PostOrPage, SettingsResponse } from '@tryghost/content-api';
import { getSettings } from '../ghost';

const postToRss = (
  posts?: PostOrPage[]
): {
  postXml: string;
  lastPostDate: string;
} => {
  let lastPostDate = '';
  let postXml = '';
  (posts || []).forEach((post) => {
    const postDate = post.created_at ? Date.parse(post.created_at) : null;
    if (!lastPostDate || (postDate && postDate > Date.parse(lastPostDate))) {
      lastPostDate = post.created_at || '';
    }
    postXml += `
      <item>
        <title>${post.title}</title>
        <link>
          ${process.env.NEXT_HOSTNAME}/post/${post.slug}
        </link>

        <pubDate>${
          post.created_at && new Date(post.created_at).toUTCString()
        }</pubDate>
        <description>
        <![CDATA[${post.html}]]>
        </description>
    </item>`;
  });
  return {
    postXml,
    lastPostDate: new Date(lastPostDate).toUTCString(),
  };
};

const getRSSFeed = (
  settings: SettingsResponse,
  posts?: PostOrPage[]
): string => {
  const { postXml, lastPostDate } = postToRss(posts);
  return `<?xml version="1.0" ?>
  <rss version="2.0">
    <channel>
        <title>${settings.title}</title>
        <link>${process.env.NEXT_HOSTNAME}</link>
        <description>${settings.description}</description>
        <language>en</language>
        <lastBuildDate>${lastPostDate}</lastBuildDate>
        ${postXml}
    </channel>
  </rss>`;
};

const Feed: NextPage = () => {
  return null;
};

export const getServerSideProps: GetServerSideProps = async ({ res }) => {
  const posts = await getPosts();
  const siteSettings = await getSettings();

  if (!posts) {
    res.statusCode = 503;
  } else {
    res.setHeader('Content-Type', 'text/xml');
    res.write(getRSSFeed(siteSettings, posts));
    res.end();
  }

  return {
    props: {},
  };
};

export default Feed;
