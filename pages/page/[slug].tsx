import * as React from 'react';
import { getSinglePage } from '../../ghost';
import { GetServerSideProps, NextPage } from 'next';
import dynamic from 'next/dynamic';
import { Box, Heading } from 'theme-ui';
import { PostOrPage } from '@tryghost/content-api';
import { parseExcerpt, parser } from '../../utils';
import { useAnalytics } from 'use-analytics';

const Custom404 = dynamic(() => import('../404'));
const Meta = dynamic(() => import('../../components/Meta'));

type PageProps = {
  page: PostOrPage | null;
};

const Page: NextPage<PageProps> = ({ page }) => {
  const analytics = useAnalytics();

  if (!page || !page.html) {
    return <Custom404 />;
  }

  /* eslint-disable @typescript-eslint/camelcase */
  const {
    title,
    twitter_title,
    twitter_description,
    twitter_image,
    meta_title,
    meta_description,
    html,
    excerpt,
    feature_image,
    og_description,
    og_image,
    og_title,
  } = page;

  React.useEffect(() => {
    analytics.page({
      title,
    });
  }, []);

  return (
    <>
      <Meta
        title={meta_title || title}
        description={meta_description || parseExcerpt(excerpt, 160)}
        twitterDescription={
          twitter_description || meta_description || parseExcerpt(excerpt, 200)
        }
        twitterImage={twitter_image || feature_image}
        twitterTitle={twitter_title || title}
        featureImage={feature_image}
        fbImage={og_image || feature_image}
        fbDescription={
          og_description || meta_description || parseExcerpt(excerpt, 200)
        }
        fbTitle={og_title || title}
      />

      <Heading as="h1">{title}</Heading>
      {html && <Box as="article">{parser(html)}</Box>}
    </>
  );
  /* eslint-enable @typescript-eslint/camelcase */
};

export const getServerSideProps: GetServerSideProps<PageProps> = async ({
  params,
  res,
}) => {
  const singlePage = await getSinglePage(params?.slug as string);

  if (!singlePage) {
    res.statusCode = 404;
  }

  return {
    props: {
      page: singlePage || null,
    },
  };
};

export default Page;
