/* eslint-disable @typescript-eslint/camelcase */
const env = {
  NODE_ENV: process.env.NODE_ENV,
  GHOST_CONTENT_API_KEY: process.env.GHOST_CONTENT_API_KEY,
  GHOST_API_URL: process.env.GHOST_API_URL || 'http://localhost:2368',
  NEXT_HOSTNAME: process.env.NEXT_HOSTNAME || 'http://localhost:3000',
  GA_TRACKING_ID: process.env.GA_TRACKING_ID || '',
  SENTRY_DSN: process.env.SENTRY_DSN,
};

module.exports = {
  apps: [
    {
      name: 'nextjs',
      script: 'server.js',
      instances: '8',
      exec_mode: 'cluster',
      node_args: '--max_old_space_size=100',
      max_memory_restart: '100M',
      env: {
        ...env,
      },
      env_production: {
        ...env,
      },
    },
  ],
};
/* eslint-enable @typescript-eslint/camelcase */
