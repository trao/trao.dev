/** @jsx jsx */
import * as React from 'react';
import { BsDot } from 'react-icons/bs';
import {
  Box,
  BoxProps,
  jsx,
  SxStyleProp,
  useColorMode,
  Container,
  Text,
} from 'theme-ui';
import Link from './Link';

type FooterProps = {} & BoxProps;

const footerStyles = (isDark: boolean): SxStyleProp => ({
  boxShadow: `inset 0px 1px 0px ${isDark ? '#292932' : '#E2E2EA'}`,
});

const Footer: React.FC<FooterProps> = (props) => {
  const [colorMode] = useColorMode();
  const isDark = colorMode === 'dark';
  return (
    <Box {...props} sx={footerStyles(isDark)} padding={3} marginTop={4}>
      <Container as="footer" sx={{ textAlign: 'center' }}>
        <Text as="small" sx={{ display: 'block' }} mb={1}>
          Thilak Rao. Website built with{' '}
          <Link href="https://nextjs.org">Next.js</Link> and{' '}
          <Link href="https://ghost.org/">Ghost</Link>
        </Text>
        <Text
          as="small"
          sx={{ justifyContent: 'center', alignItems: 'center' }}
        >
          <Link href="#top">Back to top</Link> <BsDot size={10} />{' '}
          <Link href="https://gitlab.com/trao/trao.dev">Source code</Link>{' '}
          <BsDot size={10} /> <Link href="/feed.xml">RSS Feed</Link>
        </Text>
      </Container>
    </Box>
  );
};

export default Footer;
