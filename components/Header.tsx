/** @jsx jsx */
import * as React from 'react';
import {
  Flex,
  Box,
  Image,
  Container,
  BoxProps,
  IconButton,
  jsx,
  SxStyleProp,
  useColorMode,
} from 'theme-ui';
import Link from './Link';
import { FiSun, FiMoon } from 'react-icons/fi';

const headerStyles: SxStyleProp = (isDark: boolean) => ({
  height: 70,
  padding: 3,
  bg: 'background',
  boxShadow: `inset 0px -1px 0px ${isDark ? '#292932' : '#E2E2EA'}`,
});

export type NavigationItem = {
  label: string;
  url: string;
};

export type HeaderProp = {
  navigation: NavigationItem[];
} & BoxProps;

const Header: React.FC<HeaderProp> = (props) => {
  const [colorMode, setColorMode] = useColorMode();
  const isDark = colorMode === 'dark';

  const siteNavigation = props.navigation.map((nav, index) => (
    <Box as="span" p={2} key={index}>
      <Link href={nav.url} navLink>
        {nav.label}
      </Link>
    </Box>
  ));

  return (
    <Box {...props} as="header" sx={headerStyles(isDark)}>
      <Container
        sx={{
          display: 'flex',
          justifyContent: 'space-between',
          alignItems: 'center',
        }}
      >
        <Flex
          id="top"
          sx={{
            a: {
              fontSize: 20,
              lineHeight: '28px',
              fontFamily: 'heading',
              fontWeight: 'heading',
              color: 'text',
              textDecoration: 'none',
            },
          }}
        >
          <Box>
            {
              <Link href="/">
                <Image
                  src={isDark ? '/logo-dark.svg' : '/logo.svg'}
                  width={28}
                  height={28}
                  mr={2}
                  aria-hidden
                />
              </Link>
            }
          </Box>
          <Link href="/">trao.dev</Link>
        </Flex>

        <Flex
          as="nav"
          sx={{ justifyContent: 'space-between', alignItems: 'center' }}
        >
          {siteNavigation}
          <IconButton
            marginLeft={[1, 2, 3]}
            onClick={(): void => {
              setColorMode(isDark ? 'default' : 'dark');
            }}
          >
            {isDark ? (
              <FiSun size={25} title="Toggle Theme" />
            ) : (
              <FiMoon size={25} title="Toggle Theme" />
            )}
          </IconButton>
        </Flex>
      </Container>
    </Box>
  );
};

export default Header;
