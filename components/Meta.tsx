import * as React from 'react';
import Head from 'next/head';
import { useRouter } from 'next/router';
import { Article, WithContext } from 'schema-dts';

export type MetaProps = {
  twitterImage?: string | null;
  twitterTitle?: string | null;
  twitterDescription?: string | null;
  title?: string | null;
  description?: string | null;
  featureImage?: string | null;
  fbDescription?: string | null;
  fbImage?: string | null;
  fbTitle?: string | null;
  structuredData?: WithContext<Article>;
};

/* eslint-disable sonarjs/cognitive-complexity */
const Meta: React.FC<MetaProps> = ({
  twitterImage,
  twitterTitle,
  twitterDescription,
  title,
  description,
  featureImage,
  fbDescription,
  fbImage,
  fbTitle,
  structuredData,
}) => {
  const { asPath } = useRouter();
  const fullUrl = `${process.env.NEXT_HOSTNAME}${asPath}`;

  return (
    <Head>
      <title key="pageTitle">{title}</title>
      {description ? (
        <meta name="description" content={description} key="metaDescription" />
      ) : null}

      {twitterImage || featureImage ? (
        <meta
          name="twitter:card"
          content="summary_large_image"
          key="twitterCard"
        />
      ) : null}
      <meta name="twitter:creator" content="@thilak" key="twitterCreator" />
      {twitterTitle || title ? (
        <meta
          name="twitter:title"
          content={(twitterTitle || title) as string}
          key="twitterTitle"
        />
      ) : null}
      {twitterDescription || description ? (
        <meta
          name="twitter:description"
          content={(twitterDescription || description) as string}
          key="twitterDescription"
        />
      ) : null}
      {twitterImage || featureImage ? (
        <meta
          name="twitter:image"
          content={(twitterImage || featureImage) as string}
          key="twitterImage"
        />
      ) : null}

      <meta property="og:url" content={fullUrl} key="fbUrl" />
      <meta property="og:type" content="article" key="fbType" />
      {fbTitle || title ? (
        <meta
          property="og:title"
          content={(fbTitle || title) as string}
          key="fbTitle"
        />
      ) : null}
      {fbDescription || description ? (
        <meta
          property="og:description"
          content={(fbDescription || description) as string}
          key="fbDescription"
        />
      ) : null}
      {fbImage || featureImage ? (
        <meta
          property="og:image"
          content={(fbImage || featureImage) as string}
          key="fbImage"
        />
      ) : null}
      {structuredData && (
        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify(structuredData),
          }}
        />
      )}
    </Head>
  );
};
/* eslint-enable sonarjs/cognitive-complexity */

export default Meta;
