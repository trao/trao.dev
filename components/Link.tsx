/** @jsx jsx */
import * as React from 'react';
import {
  jsx,
  Link as LinkComponent,
  NavLink,
  LinkProps as LinkComponentProps,
} from 'theme-ui';
import NextLink, { LinkProps as NextLinkProps } from 'next/link';
import { AnalyticsActions, AnalyticsCategory } from '../utils/analytics';
import { useAnalytics } from 'use-analytics';

type LinkProps = {
  navLink?: boolean;
} & NextLinkProps &
  Omit<LinkComponentProps, 'as'>;

const Link: React.FC<LinkProps> = (props) => {
  const { navLink = false, href } = props;
  const analytics = useAnalytics();
  const isExternal = href.includes('http');
  const trackExternalClick = (): void => {
    analytics.track(AnalyticsActions.CLICKED, {
      category: AnalyticsCategory.OUTBOUND_LINK,
      label: href,
    });
  };

  if (isExternal) {
    return navLink ? (
      <NavLink {...(props as LinkComponentProps)} onClick={trackExternalClick}>
        {props.children}
      </NavLink>
    ) : (
      <LinkComponent
        {...(props as LinkComponentProps)}
        onClick={trackExternalClick}
      >
        {props.children}
      </LinkComponent>
    );
  }

  return (
    <NextLink
      href={props.href}
      as={props.as}
      replace={props.replace}
      scroll={props.scroll}
      shallow={props.shallow}
      prefetch={props.prefetch}
      passHref
    >
      {navLink ? (
        <NavLink>{props.children}</NavLink>
      ) : (
        <LinkComponent>{props.children}</LinkComponent>
      )}
    </NextLink>
  );
};

export default Link;
