import * as React from 'react';
import dynamic from 'next/dynamic';
import { NavigationItem } from './Header';
import { Container, Flex } from 'theme-ui';

const Header = dynamic(() => import('./Header'));
const Footer = dynamic(() => import('./Footer'));

type LayoutProps = {
  navigation: NavigationItem[];
};

const Layout: React.FC<LayoutProps> = (props) => {
  return (
    <Flex sx={{ minHeight: '100vh', flexDirection: 'column' }}>
      <Header navigation={props.navigation} />
      <Container as="main" padding={[2, 2, 0]} sx={{ flex: 1 }}>
        {props.children}
      </Container>
      <Footer />
    </Flex>
  );
};

export default Layout;
