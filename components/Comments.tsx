/** @jsx jsx */
import * as React from 'react';
import { Box, BoxProps, jsx, useColorMode } from 'theme-ui';
import { useRouter } from 'next/router';
import { DiscussionEmbed } from 'disqus-react';
import { useInView } from 'react-intersection-observer';
import { AnalyticsActions, AnalyticsCategory } from '../utils/analytics';
import { useAnalytics } from 'use-analytics';

export type CommentsProps = {
  shortname: string;
  uuid: string;
  title: string;
} & BoxProps;

const Comments: React.FC<CommentsProps> = (props) => {
  const { asPath } = useRouter();
  const [componentLoaded, setComponentLoaded] = React.useState(false);
  const [colorMode] = useColorMode();
  const [ref, inView] = useInView({ triggerOnce: true });
  const analytics = useAnalytics();
  const isDark = colorMode === 'dark';
  const fullUrl = `${process.env.NEXT_HOSTNAME}${asPath}`;
  const { shortname, title, uuid } = props;

  React.useEffect(() => {
    setComponentLoaded(true);
  }, []);

  React.useEffect(() => {
    if (inView) {
      analytics.track(AnalyticsActions.VIEWED, {
        category: AnalyticsCategory.COMMENTS,
        label: title,
      });
    }
  }, [inView]);

  return (
    <Box bg={isDark ? 'backgroundLight' : ''} {...props} ref={ref}>
      {componentLoaded && (
        <DiscussionEmbed
          shortname={shortname}
          config={{
            url: fullUrl,
            identifier: uuid,
            title: title,
          }}
        />
      )}
    </Box>
  );
};

export default Comments;
