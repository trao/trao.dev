/** @jsx jsx */
import * as React from 'react';
import { jsx, Embed as EmbedComponent, EmbedProps } from 'theme-ui';
import { useInView } from 'react-intersection-observer';
import { AnalyticsActions, AnalyticsCategory } from '../utils/analytics';
import { useAnalytics } from 'use-analytics';

const Embed: React.FC<EmbedProps> = (props) => {
  const [ref, inView] = useInView({ triggerOnce: true });
  const analytics = useAnalytics();

  React.useEffect(() => {
    if (inView) {
      analytics.track(AnalyticsActions.VIEWED, {
        category: AnalyticsCategory.EMBEDDED_OBJECT,
        label: props.src,
      });
    }
  }, [inView]);

  return (
    <div ref={ref}>
      {inView && (
        <EmbedComponent {...(props as AnyType)}>
          {props.children}
        </EmbedComponent>
      )}
    </div>
  );
};

export default Embed;
