export const parseExcerpt = (excerpt?: string, charCount = 500): string => {
  const parsedString = (excerpt || '')
    .replace(/(\[.*?])/g, '')
    .replace(/ ,/g, ',')
    .slice(0, Math.max(0, charCount))
    .replace(/(\r\n|\n|\r)/gm, '');

  return excerpt !== '' ? parsedString + '...' : excerpt;
};
