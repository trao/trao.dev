import { v4 } from 'uuid';

type Cookies = {
  sid: string;
};

export const cookiesToJson = (cookies: string): Cookies => {
  const json = cookies.split('; ').reduce((previous: AnyType, current) => {
    const [name, value] = current.split('=');
    previous[name] = value;
    return previous;
  }, {}) as Cookies;

  return json || { sid: v4() };
};
