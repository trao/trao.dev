import * as React from 'react';
import parse, { domToReact } from 'html-react-parser';
import { Heading, Text, Image, Box } from 'theme-ui';
import Embed from '../components/Embed';
import Link from '../components/Link';

/* eslint-disable sonarjs/cognitive-complexity */
const replaceWithReact = (
  domNode: AnyType
): React.FunctionComponentElement<AnyType> | void => {
  // Replace links
  if (domNode.name === 'a' && domNode.attribs.href) {
    return React.createElement(
      Link,
      { href: domNode.attribs.href },
      domToReact(domNode.children, { replace: replaceWithReact })
    );
  }

  // Typography replacement
  if (domNode.name === 'h1') {
    return React.createElement(
      Heading,
      { as: 'h1' },
      domToReact(domNode.children, { replace: replaceWithReact })
    );
  }

  if (domNode.name === 'h2') {
    return React.createElement(
      Heading,
      { as: 'h2' },
      domToReact(domNode.children, { replace: replaceWithReact })
    );
  }

  if (domNode.name === 'h3') {
    return React.createElement(
      Heading,
      { as: 'h3' },
      domToReact(domNode.children, { replace: replaceWithReact })
    );
  }

  if (domNode.name === 'h4') {
    return React.createElement(
      Heading,
      { as: 'h4' },
      domToReact(domNode.children, { replace: replaceWithReact })
    );
  }

  if (domNode.name === 'h5') {
    return React.createElement(
      Heading,
      { as: 'h5' },
      domToReact(domNode.children, { replace: replaceWithReact })
    );
  }

  if (domNode.name === 'h6') {
    return React.createElement(
      Heading,
      { as: 'h6' },
      domToReact(domNode.children, { replace: replaceWithReact })
    );
  }

  if (domNode.name === 'p') {
    return React.createElement(
      Text,
      { as: 'p', mb: 3 },
      domToReact(domNode.children, { replace: replaceWithReact })
    );
  }

  // Replace images
  if (domNode.name === 'img' && domNode.attribs.src) {
    return React.createElement(Image, { src: domNode.attribs.src });
  }

  // Replace lists
  if (domNode.name === 'ul') {
    return React.createElement(
      Text,
      { as: 'ul' },
      domToReact(domNode.children, { replace: replaceWithReact })
    );
  }
  if (domNode.name === 'ol') {
    return React.createElement(
      Text,
      { as: 'ol' },
      domToReact(domNode.children, { replace: replaceWithReact })
    );
  }

  // Embeds
  if (domNode.name === 'iframe' && domNode.attribs.src) {
    return React.createElement(Embed, {
      marginBottom: 2,
      marginTop: 4,
      src: domNode.attribs.src,
    });
  }

  if (domNode.name === 'figure') {
    return React.createElement(
      Box,
      { as: 'figure' },
      domToReact(domNode.children, { replace: replaceWithReact })
    );
  }
};
/* eslint-enable sonarjs/cognitive-complexity */

export const parser = (html: string): JSX.Element | JSX.Element[] => {
  return parse(html, {
    replace: replaceWithReact,
  });
};
