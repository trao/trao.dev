import axios from 'axios';
import { Analytics } from 'analytics';

export enum AnalyticsActions {
  VIEWED = 'viewed',
  CLICKED = 'clicked',
}

export enum AnalyticsCategory {
  INTERNAL_LINK = 'internal_link',
  OUTBOUND_LINK = 'outbound_link',
  COMMENTS = 'comments',
  EMBEDDED_OBJECT = 'embedded_object',
  ERROR_404 = 'error_404',
}

export type AnalyticsPayload = {
  payload: {
    type: string;
    event: string;
    properties: {
      category: string;
      label: string;
    };
    options: AnyType;
    userId: string;
    anonymousId: string;
    meta: {
      timestamp: number;
    };
  };
  abort?: (reason: string, plugins: AnyType) => void;
  config?: AnyType;
};

/**
 * Recursively lowercase all values in an object
 * @param object
 */
const recursiveLowerCase = <T extends object>(object: T): T => {
  if (Array.isArray(object)) {
    return (object.map((value) => recursiveLowerCase(value)) as unknown) as T;
  }

  if (object !== null && object.constructor === Object) {
    return Object.keys(object).reduce(
      (result, key) => ({
        ...result,
        [key]: recursiveLowerCase<T>((object as AnyType)[key].toLowerCase()),
      }),
      {}
    ) as T;
  }

  return object;
};

const customEnrichmentPlugin = {
  NAMESPACE: 'custom-enrichment-plugin',
  trackStart: ({ payload, abort }: AnalyticsPayload): AnalyticsPayload => {
    payload.properties = recursiveLowerCase(payload.properties);

    return { payload, abort };
  },
};

const endpoint = '/api/t';
function customAnalyticsPlugin() {
  return {
    NAMESPACE: 'custom-analytics-plugin',
    page: ({ payload }: AnalyticsPayload): void => {
      axios.post(endpoint, payload);
    },
    identify: ({ payload }: AnalyticsPayload): void => {
      axios.post(endpoint, payload);
    },
    track: ({ payload }: AnalyticsPayload): void => {
      axios.post(endpoint, payload);
    },
  };
}

export const analytics = Analytics({
  debug: process.env.NODE_ENV !== 'production',
  plugins: [customEnrichmentPlugin, customAnalyticsPlugin()],
});
