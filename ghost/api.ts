import GhostContentAPI, { GhostAPI } from '@tryghost/content-api';

// eslint-disable-next-line @typescript-eslint/ban-ts-ignore
// @ts-ignore TODO: Fix this bug in @types/tryghost__content-api
export const api: GhostAPI = new GhostContentAPI({
  url: process.env.GHOST_API_URL as string,
  key: process.env.GHOST_CONTENT_API_KEY || '',
  version: 'v3',
});
