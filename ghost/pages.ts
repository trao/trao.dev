import { api } from './api';
import { PostOrPage } from '@tryghost/content-api';

export async function getPages(): Promise<PostOrPage[] | void> {
  return (await api.pages
    .browse({
      limit: 'all',
    })
    .catch((error: Error) => {
      console.error(error);
    })) as PostOrPage[] | void;
}

export async function getSinglePage(
  postSlug: string
): Promise<PostOrPage | void> {
  return await api.pages
    .read({
      slug: postSlug,
    })
    .catch((error: Error) => {
      console.error(error);
    });
}
