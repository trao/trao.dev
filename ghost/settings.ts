import { api } from './api';
import { SettingsResponse } from '@tryghost/content-api';

export async function getSettings(): Promise<SettingsResponse> {
  return (await api.settings
    .browse({
      limit: 'all',
    })
    .catch((error: Error) => {
      console.error(error);
    })) as SettingsResponse;
}
