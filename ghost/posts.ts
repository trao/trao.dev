import { api } from './api';
import { PostOrPage } from '@tryghost/content-api';

export async function getPosts(): Promise<PostOrPage[] | void> {
  return (await api.posts
    .browse({
      limit: 'all',
    })
    .catch((error: Error) => {
      console.error(error);
    })) as PostOrPage[] | void;
}

export async function getSinglePost(
  postSlug: string
): Promise<PostOrPage | void> {
  return await api.posts
    .read(
      {
        slug: postSlug,
      },
      {
        include: ['tags', 'authors'],
      }
    )
    .catch((error: Error) => {
      console.error(error);
    });
}
