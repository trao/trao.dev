/* eslint-disable @typescript-eslint/no-var-requires */
const webpack = require('webpack');
const withSourceMaps = require('@zeit/next-source-maps');
/* eslint-enable @typescript-eslint/no-var-requires */

module.exports = withSourceMaps({
  poweredByHeader: false,
  env: {
    GHOST_CONTENT_API_KEY: process.env.GHOST_CONTENT_API_KEY,
    GHOST_API_URL: process.env.GHOST_API_URL || 'http://localhost:2368',
    NEXT_HOSTNAME: process.env.NEXT_HOSTNAME || 'http://localhost:3000',
    GA_TRACKING_ID: process.env.GA_TRACKING_ID || '',
    SENTRY_DSN: process.env.SENTRY_DSN,
  },
  webpack: (config, { isServer, buildId }) => {
    config.plugins.push(
      new webpack.DefinePlugin({
        'process.env.SENTRY_RELEASE': JSON.stringify(buildId),
      })
    );

    if (!isServer) {
      config.resolve.alias['@sentry/node'] = '@sentry/browser';
    }

    return config;
  },
});
