# trao.dev

Business card / nameplate web site power by [Ghost](https://ghost.org/) and [Next.js](https://nextjs.org/)

## Available scripts

### Yarn

- **`yarn dev`**: Start next.js dev server.
- **`yarn build`**: Build next.js frontend.
- **`yarn start`**: Start next.js server.
- **`yarn lint`**: Run [ESlint](https://eslint.org/)
- **`yarn format`**: Run [Prettier](https://prettier.io/)

### Makefile

- **`make start`**: Start ghost in a docker container.
- **`make setup`**: Setup config file and volume for ghost.
- **`make login`**: Login into Gitlab Docker registry.
- **`make docker-build`**: Build docker image from Dockerfile for the next.js frontend.
- **`make docker-run`**: Build and run next.js frontend.
- **`make docker-push`**: Build and push docker image containing next.js frontend to Docker registry.
- **`make docker-stop`**: Kill and remove next.js docker container

## Env variables

Add these variable to your environment.

- **`DATA_DIR`**: Absolute path of the director to store data files.
- **`GHOST_CONTAINER_NAME`**: Name of the container running Ghost.
- **`GHOST_CONTENT_API_KEY`**: Content API key of Ghost.
- **`GHOST_URL`**: URL of Ghost
- **`GHOST_API_URL`**: API URL of Ghost (default `http://localhost:2368`)
- **`NEXTJS_CONTAINER_NAME`**: Name of the Next.js container.
- **`DOCKER_IMAGE`**: Name of the docker image.
- **`DOCKER_IMAGE_TAG`**: Tag of the docker image.
- **`NEXT_HOSTNAME`**: Hostname of the Next.js frontend app (default `http://localhost:3000`)
- **`GA_TRACKING_ID`**: Google Analytics tracking id (example `UA-xxxxxxxxx-1`)
- **`SENTRY_DSN`**: To send data to Sentry you will need to configure your Sentry DSN

## Generate Self-Signed Certificate for Development

For SSL during local development, you'll need a self signed SSL certificate. Install mkcert on your development machine and run this script:

```bash
sh self-signed.sh
```

## License

This project is MIT-licensed. Fork and reuse the code, update the content to your own
