// eslint-disable-next-line @typescript-eslint/no-explicit-any
type AnyType = any;

interface ObjectWithParams<T = FixMeLaterType> {
  readonly [key: string]: T;
}

declare module '@theme-ui/presets';
declare module '@theme-ui/core';
declare module '@analytics/google-analytics';
declare module 'use-analytics' {
  const AnalyticsInstance = import('analytics').AnalyticsInstance;
  const React = import('react').React;
  export const useAnalytics = (): AnalyticsInstance => AnalyticsInstance;
  export const AnalyticsProvider: React.FC<{
    instance: AnalyticsInstance;
  }>;
}
