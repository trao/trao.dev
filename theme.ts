import { base } from '@theme-ui/presets';
import { merge } from '@theme-ui/core';

const heading = {
  fontFamily: 'heading',
  lineHeight: 'heading',
  fontWeight: 'heading',
  marginBottom: 2,
  marginTop: 4,
};

export const theme = merge(base, {
  useColorSchemeMediaQuery: true,
  colors: {
    background: '#FFF',
    backgroundLight: '#FFF',
    muted: '#979797',
    primary: '#0062FF',
    secondary: '#FFC542',
    text: '#171725',
    white: '#FAFAFB',
    modes: {
      dark: {
        background: '#13131A',
        backgroundLight: '#1C1C24',
        muted: '#696974',
        primary: '#1E75FF',
        secondary: '#FFC542',
        text: '#FAFAFB',
      },
    },
  },
  radii: [0, 4, 8, 16, 32, 64],
  fonts: { body: "'Roboto', sans-serif", heading: "'Poppins', sans-serif" },
  fontSizes: [12, 14, 16, 18, 24, 28, 36, 48],
  fontWeights: {
    body: 400,
    heading: 600,
    bold: 700,
  },
  sizes: {
    container: 800,
  },
  text: {
    caps: {
      textTransform: 'uppercase',
    },
    subhead: {
      fontFamily: 'heading',
      lineHeight: 'heading',
      fontWeight: 'heading',
      fontSize: 3,
    },
    muted: {
      fontSize: 1,
      color: 'muted',
    },
    heading,
  },
  images: {
    avatar: {
      width: 48,
      height: 48,
      borderRadius: 99999,
    },
  },
  cards: {
    primary: {
      bg: 'background',
      borderRadius: 3,
      boxShadow: '0 0 8px rgba(0, 0, 0, 0.125)',
    },
  },
  styles: {
    root: {
      fontFamily: 'body',
      fontWeight: 'normal',
      backgroundColor: 'backgroundLight',
    },
    a: {
      color: 'primary',
      textDecoration: 'none',
    },
    h1: {
      ...heading,
      fontSize: 48,
    },
    h2: {
      ...heading,
      fontSize: 36,
    },
    h3: {
      ...heading,
      fontSize: 28,
    },
    h4: {
      ...heading,
      fontSize: 24,
    },
    h5: {
      ...heading,
      fontSize: 18,
    },
  },
});
