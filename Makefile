include .env
export $(shell sed 's/=.*//' .env)

# build app image
docker-build: ./Dockerfile
	docker build . --pull -t $(IMAGE_NAME)

docker-push: docker-build login
	docker push $(IMAGE_NAME)

docker-run: docker-build
	docker run -d -p 3000:3000 --name $(NEXTJS_CONTAINER_NAME) -e GHOST_CONTENT_API_KEY=$(GHOST_CONTENT_API_KEY) -e GHOST_API_URL=$(GHOST_API_URL) $(IMAGE_NAME)

docker-kill:
	docker kill $(NEXTJS_CONTAINER_NAME)

docker-stop: docker-kill
	docker rm $(NEXTJS_CONTAINER_NAME)

login:
	docker login registry.gitlab.com

setup:
	sh setup.sh

start: setup
	sh start.sh

# Get the username of the user running make.
USERNAME ?= $(shell ( [ -f /etc/username ] && cat /etc/username  ) || whoami)

# Try to detect current branch if not provided from environment
BRANCH ?= $(shell git rev-parse --abbrev-ref HEAD)

# Commit hash from git
COMMIT=$(shell git rev-parse --short HEAD)

IMAGE_NAME=registry.gitlab.com/trao/trao.dev:latest

# Nextjs container name
NEXTJS_CONTAINER_NAME=nextjs-frontend